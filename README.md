# README #

This app was created with Twilio and Python, for send text and get a response in WhatsApp.

### What is this repository for? ###

* Send and receive text from WhatsApp.
* Version 1.0
* Helped by [Twilio](https://www.twilio.com/)

### How do I get set up? ###

* Create account on Twilio.
* Configurate your phone number.
* Join to group server in WhatsApp link - Add number in Twilio.
* Start server with Flask.
* Send text and test results.

### Contribution guidelines ###

* [Jorge Martinez](https://coke680.bitbucket.io/)
* Helped by [Twilio](https://www.twilio.com/)