from flask import Flask, request, redirect, url_for, flash
from flask_ngrok import run_with_ngrok
import requests
import xmltodict
import pprint
import json
#from twilio.twiml.messaging_response import MessagingResponse
from twilio.twiml.messaging_response import Body, Message, Redirect, MessagingResponse

app = Flask(__name__)
run_with_ngrok(app)


@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == "POST":
        incoming_msg = request.values.get('Body', '').lower()
        resp = MessagingResponse()
        msg = resp.message()

        if incoming_msg == '1':
            return redirect(url_for('opcion{}'.format(incoming_msg)))
        if incoming_msg == '2':
            return redirect(url_for('opcion{}'.format(incoming_msg)))
        if incoming_msg == '3':
            return redirect(url_for('opcion{}'.format(incoming_msg)))
        if incoming_msg == 'ayuda':
            return redirect(url_for('ayuda'))

        if 'hola' in incoming_msg:
            msg.body('Este es un asistente automático:\n ¿Qué desea?\n 1) Consultar\n 2) Hablar con alguien :) \n 3) Huevear \n\n Escribe "ayuda" para más información')
        elif 'consulta' in incoming_msg:
            msg.body('Este es un asistente automático:\n ¿Qué desea?\n 1) Consultar\n 2) Hablar con alguien :) \n 3) Huevear \n\n Escribe "ayuda" para más información')
        elif 'hello' in incoming_msg:
            msg.body('Este es un asistente automático:\n ¿Qué desea?\n 1) Consultar\n 2) Hablar con alguien :) \n 3) Huevear \n\n Escribe "ayuda" para más información')
        elif 'alo' in incoming_msg:
            msg.body('Este es un asistente automático:\n ¿Qué desea?\n 1) Consultar\n 2) Hablar con alguien :) \n 3) Huevear \n\n Escribe "ayuda" para más información')
        elif 'hola' in incoming_msg:
            msg.body('Este es un asistente automático:\n ¿Qué desea?\n 1) Consultar\n 2) Hablar con alguien :) \n 3) Huevear \n\n Escribe "ayuda" para más información')
        else:
            msg.body('Hola! ¿Cómo estás? ¡Salúdame y te ayudaré!')

        return str(resp)   

    # extracting data in json format
    URL = "https://AC2f98e07a1dd77c6c05651c83b6bf63a3:210aa9a6a893737ddd8abdadb8ab5810@api.twilio.com/2010-04-01/Accounts/AC2f98e07a1dd77c6c05651c83b6bf63a3/Balance"  
    # location given here 
    r = requests.get(url = URL)
    x = xmltodict.parse(r.content)
    j = json.dumps(x)
    j = json.loads(j)

    balance = j['TwilioResponse']['Balance']['Balance']

    return str(balance)
    #return str(resp), redirect(url_for('primera'))
    #return redirect(url_for('.do_foo', messages=messages))

@app.route('/opcion1', methods=['GET', 'POST'])
def opcion1():
    resp = MessagingResponse()
    msg = resp.message()
    message = 'Hola, seleccionaste opción 1'
    msg.body(message)
    return str(resp)

@app.route('/opcion2', methods=['GET','POST'])
def opcion2():
    resp = MessagingResponse()
    msg = resp.message()
    message = 'Hola, seleccionaste opción 2'
    msg.body(message)
    return str(resp)

@app.route('/opcion3', methods=['GET', 'POST'])
def opcion3():
    resp = MessagingResponse()
    msg = resp.message()
    message = 'Hola, seleccionaste opción 3'
    msg.body(message)
    return str(resp)

@app.route('/ayuda', methods=['GET', 'POST'])
def ayuda():
    resp = MessagingResponse()
    msg = resp.message()
    message = 'Debes contactar a tu profesor'
    msg.body(message)
    return str(resp)

if __name__ == '__main__':
    app.run()